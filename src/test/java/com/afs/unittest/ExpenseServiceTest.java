package com.afs.unittest;

import com.afs.unittest.Project.Project;
import com.afs.unittest.Project.ProjectType;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import com.afs.unittest.expense.ExpenseType;
import org.junit.jupiter.api.Test;

import static com.afs.unittest.expense.ExpenseType.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(new Project(ProjectType.INTERNAL, "Project A"));
        // then
        assertEquals(INTERNAL_PROJECT_EXPENSE, expenseType);
    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(new Project(ProjectType.EXTERNAL, "Project A"));
        // then
        assertEquals(EXPENSE_TYPE_A, expenseType);

    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(new Project(ProjectType.EXTERNAL, "Project B"));
        // then
        assertEquals(EXPENSE_TYPE_B, expenseType);

    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        ExpenseService expenseService = new ExpenseService();
        // when
        ExpenseType expenseType = expenseService.getExpenseCodeByProject(new Project(ProjectType.EXTERNAL, "Other Name"));
        // then
        assertEquals(OTHER_EXPENSE, expenseType);

    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        ExpenseService expenseService = new ExpenseService();
        // then
        assertThrows(UnexpectedProjectTypeException.class, () -> {
            // when
            expenseService.getExpenseCodeByProject(new Project(ProjectType.UNEXPECTED_PROJECT_TYPE, ""));
        });


    }
}